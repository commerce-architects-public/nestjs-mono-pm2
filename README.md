<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
<a href="https://pm2.keymetrics.io/" target="blank"><img src="https://miro.medium.com/max/1400/1*6H32xsC9B1leD4kn_sItCg.jpeg" width="150"/></a>
</p>

[[_TOC_]]

## Description

POC of [NestJS](https://github.com/nestjs/nest) monorepos with multiple application running in a single docker container using [pm2](https://pm2.keymetrics.io/).

The idea is to run multiple independent NestJS apps in a single docker container as a separate process using `pm2` and expose each one as a different port to the outside world, so that after building & running the docker container you could access each app.

> Note: WHY would we want to use `pm2` and not deploy each one of our app as a single Docker container? That is a good question, one of our client's requirements is to run multiple apps in a single Docker container due to some hard requirements where the runtime environnement is shared.

In this POC, we'll create a hypothetical NestJS applications with multiple Microservices organized as `Monorepos`.

> Note: For more info about NestJS monorepos please refer [here](https://docs.nestjs.com/cli/monorepo).

Our NestJS Monorepos app will consist of the following Microservices:

- Users Microservice - addressable via: http://localhost:3000
- Personalization Microservice - addressable via: http://localhost:4000
- OrderHistory Microservice - addressable via: http://localhost:5000
- HealthCheck Microservice - addressable via: http://localhost:6000/health
- Common-Lib: to be shared amongst the above Microservices and manage common code.

Besides modifying the port# for each generated app we wont actually implement any business logic since the point of this POC is to show how multiple microservices is managed by `pm2` in a Docker container.

## Pre-Reqs
- Node v14.18.0
- [nvm](https://github.com/nvm-sh/nvm) (allows you to quickly switch between versions)

## Quick Start

For the impatient:
```bash
$ nvm use
$ npm i -g @nestjs/cli
$ npm install pm2 -g
$ git clone https://gitlab.com/commerce-architects/nestjs-mono-pm2.git
$ cd nestjs-mono-pm2
$ npm run build:all
$ npm run build:docker
$ npm run pm2:run
```

- In another terminal start the pm2 monitoring:

```bash
$ npm run pm2:monitor
```

- In another terminal issue a curl command:

```bash
$ curl http://localhost:3000
$ curl http://localhost:4000
$ curl http://localhost:5000
$ curl http://localhost:6000/health
```

## Starting from Scratch

I've already created this app but wanted to outline HOW it was created...you dont need to run these steps, but documenting it anyways.

- Ensure that you have `nest` cli installed: `npm i -g @nestjs/cli`

- Created a new monorepos NestJS app:

```bash
nest new nestjs-mono-pm
cd nestjs-mono-pm
nest generate app users
nest generate app personalization
nest generate app order-history
nest generate library
```

- After generating our project's directory structure will look like this:

```bash
.
├── Dockerfile
├── README.md
├── apps
│   ├── nestjs-mono-pm2
│   │   ├── src
│   │   │   ├── app.controller.spec.ts
│   │   │   ├── app.controller.ts
│   │   │   ├── app.module.ts
│   │   │   ├── app.service.ts
│   │   │   └── main.ts
│   │   ├── test
│   │   │   ├── app.e2e-spec.ts
│   │   │   └── jest-e2e.json
│   │   └── tsconfig.app.json
│   ├── order-history
│   │   ├── src
│   │   │   ├── main.ts
│   │   │   ├── order-history.controller.spec.ts
│   │   │   ├── order-history.controller.ts
│   │   │   ├── order-history.module.ts
│   │   │   └── order-history.service.ts
│   │   ├── test
│   │   │   ├── app.e2e-spec.ts
│   │   │   └── jest-e2e.json
│   │   └── tsconfig.app.json
│   ├── personalization
│   │   ├── src
│   │   │   ├── main.ts
│   │   │   ├── personalization.controller.spec.ts
│   │   │   ├── personalization.controller.ts
│   │   │   ├── personalization.module.ts
│   │   │   └── personalization.service.ts
│   │   ├── test
│   │   │   ├── app.e2e-spec.ts
│   │   │   └── jest-e2e.json
│   │   └── tsconfig.app.json
│   └── users
│       ├── src
│       │   ├── main.ts
│       │   ├── users.controller.spec.ts
│       │   ├── users.controller.ts
│       │   ├── users.module.ts
│       │   └── users.service.ts
│       ├── test
│       │   ├── app.e2e-spec.ts
│       │   └── jest-e2e.json
│       └── tsconfig.app.json
├── libs
│   └── common-lib
│       ├── src
│       │   ├── common-lib.module.ts
│       │   ├── common-lib.service.spec.ts
│       │   ├── common-lib.service.ts
│       │   └── index.ts
│       └── tsconfig.lib.json
├── nest-cli.json
├── package-lock.json
├── package.json
├── pm2-monit.png
├── process.yml
├── tsconfig.build.json
└── tsconfig.json
```

- cd to each app and updated `main.ts` with different ports (3000, 4000, 5000) respectively to ensure that we can start all these NestJS app at the same time and not run into port conflicts.

## Installing PM2

```bash
npm i -g pm2
```

## Creating pm2 process.yml

PM2 supports multiple ways of configuring applications, for this POC i went with `process.yml` as I found some examples online when researching how to run multiple Node processes.

> Note: We've added two extra params: `max_memory_restart: "80M"` and `node_args: "--max_old_space_size=70"` to bound our node processes to a specific MAX memory constraints. See [PM2 Restart strategies](https://pm2.keymetrics.io/docs/usage/restart-strategies/) and V8 heap options: [V8 Options](https://futurestud.io/tutorials/pm2-how-to-start-your-app-with-node-js-v8-arguments)

> Note2: We Configured `users` service as a cluster with 2 instances to show how `pm2` can handle node apps as cluster without any code change.

## Implementing Healthcheck

In the default app `nodejs-mono` we have implemented custom health check service using `axios`.
Our custom healthcheck will ping all 3 services and if they all return OK returns back a JSON with 200 response like:

```bash
$ http://localhost:6000/health
[
    {
        "status": 200,
        "service": "users"
    },
    {
        "status": 200,
        "service": "personalization"
    },
    {
        "status": 200,
        "service": "order-history"
    }
]
```

## Healthcheck Dockerfile

We'll add a custom `HEALTHCHECK` directive to our Dockerfile to enable Docker Healthcheck.
It will call our curl command and fail if it gets anything other than 200 response.

```bash
HEALTHCHECK --interval=5s --timeout=10s --start-period=5s \
    CMD curl --fail http://localhost:6000/health || exit 1
```

And we can then check the status of container by issuing this command.
This will enable container orchestration services to take advantage of our docker container's health.
LoadBalancers can always call the health endpoint directly as well, but this is just another option.

```bash
$ docker inspect --format='{{json .State.Health}}' nestjs-pm2
```

## Creating Dockerfile

Next we'll create a docker file, this is a simple configuration for demonstration purposes, it copies the `dist` folder along with package\* files and then runs `npm` to only install the runtime dependencies.

```bash
FROM node:alpine
WORKDIR /app
RUN npm install pm2 -g
COPY process.yml package.json package-lock.json /app/
COPY dist /app/dist
RUN npm i --only=production
EXPOSE 3000 4000 5000
CMD ["pm2-runtime", "process.yml"]
```

We'll use `pm2-runtime` to run `process.yml`.

## Build & Run Docker container

First we'll build our apps

```bash
$ cd nestjs-mono-pm
$ nest build users && nest build personalization && nest build order-history
```

Next we'll build our Dockerfile

```bash
$ docker build -t nestjs-pm2 .
```

And finally we'll run it:

```bash
$ docker run -p 3000:3000 -p 4000:4000 -p 5000:5000 --name  nestjs-pm2 nestjs-pm2
```

Monitor your app with `pm2 monitor`

Access your NestJS apps running in the container:

```bash
$ curl http://localhost:3000
$ Hello World! from users
$ curl http://localhost:4000
$ Hello World! from personalization
$ curl http://localhost:5000
$ Hello World! from order-history
```

```bash
 $ docker exec -it nestjs-pm2 pm2 monit
```

![](pm2-monit.png)

---

# Load Testing:

To test that pm2 actually restarts our node process when it reaches `80M`, we can put some load on one of the endpoints.
We'll use `ab` [Apache Benchmark](https://httpd.apache.org/docs/2.4/programs/ab.html) which is pre-installed on Mac already:

- build and start the docker container:

```bash
$ npm run build:all
$ npm run build:docker
$ npm run pm2:run
```

- In another terminal start the pm2 monitoring:

```bash
$ npm run pm2:monitor
```

- In another terminal add some load:

```bash
$ ab -n 1000000 -c 100 http://localhost:3000/
```

- Switch back to the monitor terminal, use down-arrow button to select the 'user' app and watch the logs, as the CPU & Memory spike to over 100MB `pm2` will restart the app and memory will be back down to `70MB`.

---

# NPM Script Shortcuts:

build:

```bash
$ npm run build:all
$ npm run build:docker
```

start docker:

```bash
$ npm run pm2:run
```

list pm2 processes:

```bash
$ npm run pm2:ls
```

monitor pm2:

```bash
$ npm run pm2:monitor
```

restart pm2 without restart of docker container

```bash
npm run pm2:restart-all
```

restart individual apps

```bash
$ npm run pm2:restart-users
$ npm run pm2:restart-personalization
$ npm run pm2:order-history
```

monitor:

```bash
npm run pm2
```

stop:

```bash
$ npm run pm2:stop
```

load test:

```bash
 ab -n 1000000 -c 100 http://localhost:3000/
```

---

# Alternatives (Using `forever` as process manager)

There is yet another process manager out there which is in maintenance mode and it is called `forever`.
You can find the documentation here https://www.npmjs.com/package/forever.

`forever` manages node process(es) similar to `pm2` but misses a lot of the features from `pm2`. It is very basic in

- its support of multi-app
- doesn't support cluster mode
- cannot pass individual ENV params to each process
- cannot run as daemon mode in Docker env and in foreground mode stopping one service kills the entire service. etc.

Nonetheless, we'll all show how to run the same app using `forever` node process manager, we need to run the following commands:

```bash
$ npm run build:all
$ npm run build:docker-forever
$ npm run forever:run
```

Once the service is up we can list the apps:

```bash
$ npm run forever:ls
```

We can check the health of the service:

```bash
$ curl http://localhost:6000/health
```
