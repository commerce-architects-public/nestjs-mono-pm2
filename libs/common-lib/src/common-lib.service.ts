import { Injectable } from '@nestjs/common';

@Injectable()
export class CommonLibService {
  public async authenticate(
    username: string,
    password: string,
  ): Promise<boolean> {
    console.log(`authenticated user: ${username} password: ${password}`);
    return true;
  }
}
