import { Injectable } from '@nestjs/common';
import { ok } from 'assert';
import axios from 'axios';
const SERVICES = [
  'http://localhost:3000/health',
  'http://localhost:4000/health',
  'http://localhost:5000/health',
];
export interface Health {
  service: string;
  status: number;
}
export type ServiceHealth = Health[];

@Injectable()
export class AppService {
  async healthCheck(): Promise<ServiceHealth> {
    const serviceHealth: ServiceHealth = [];
    await axios
      .all(SERVICES.map((endpoint) => axios.get(endpoint)))
      .then(
        axios.spread((users, personalization, orderHistory) => {
          //console.log({ users, personalization, orderHistory });
          console.log('users service healthy! ' + users.status);
          console.log('persn service healthy! ' + personalization.status);
          console.log('orders service healthy! ' + orderHistory.status);
          serviceHealth.push({ status: users.status, service: 'users' });
          serviceHealth.push({
            status: personalization.status,
            service: 'personalization',
          });
          serviceHealth.push({
            status: orderHistory.status,
            service: 'order-history',
          });
        }),
      )
      .catch((errors) => {
        throw errors;
      });
    return serviceHealth;
  }
}
