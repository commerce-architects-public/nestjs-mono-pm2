import { Controller, Get } from '@nestjs/common';
import { AppService, ServiceHealth } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/health')
  async healthCheck(): Promise<ServiceHealth> {
    console.log('checking app health...');
    return await this.appService.healthCheck();
  }
}
