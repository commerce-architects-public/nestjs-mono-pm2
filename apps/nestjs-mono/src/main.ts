import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  (app as any).httpAdapter.instance.set('json spaces', 2);
  await app.listen(6000);
}
bootstrap();
