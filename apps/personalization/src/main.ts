import { NestFactory } from '@nestjs/core';
import { PersonalizationModule } from './personalization.module';

async function bootstrap() {
  const app = await NestFactory.create(PersonalizationModule);
  await app.listen(4000);
}
bootstrap();
