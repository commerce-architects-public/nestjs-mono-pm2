import { Test, TestingModule } from '@nestjs/testing';
import { PersonalizationController } from './personalization.controller';
import { PersonalizationService } from './personalization.service';

describe('PersonalizationController', () => {
  let personalizationController: PersonalizationController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [PersonalizationController],
      providers: [PersonalizationService],
    }).compile();

    personalizationController = app.get<PersonalizationController>(PersonalizationController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(personalizationController.getHello()).toBe('Hello World!');
    });
  });
});
