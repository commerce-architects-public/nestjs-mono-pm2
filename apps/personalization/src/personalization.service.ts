import { Injectable } from '@nestjs/common';

@Injectable()
export class PersonalizationService {
  getHello(): string {
    return 'Hello World From Personalization!';
  }
}
