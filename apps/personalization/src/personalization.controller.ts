import { Controller, Get } from '@nestjs/common';
import { PersonalizationService } from './personalization.service';

@Controller()
export class PersonalizationController {
  constructor(
    private readonly personalizationService: PersonalizationService,
  ) {}

  @Get()
  getHello(): string {
    return this.personalizationService.getHello();
  }
  @Get('/health')
  health() {
    return { health: 'ok' };
  }
}
