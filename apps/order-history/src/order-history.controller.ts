import { Controller, Get } from '@nestjs/common';
import { OrderHistoryService } from './order-history.service';

@Controller()
export class OrderHistoryController {
  constructor(private readonly orderHistoryService: OrderHistoryService) {}

  @Get()
  getHello(): string {
    return this.orderHistoryService.getHello();
  }
  @Get('/health')
  health() {
    return { health: 'ok' };
  }
}
