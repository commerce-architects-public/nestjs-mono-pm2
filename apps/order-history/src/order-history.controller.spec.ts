import { Test, TestingModule } from '@nestjs/testing';
import { OrderHistoryController } from './order-history.controller';
import { OrderHistoryService } from './order-history.service';

describe('OrderHistoryController', () => {
  let orderHistoryController: OrderHistoryController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [OrderHistoryController],
      providers: [OrderHistoryService],
    }).compile();

    orderHistoryController = app.get<OrderHistoryController>(OrderHistoryController);
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      expect(orderHistoryController.getHello()).toBe('Hello World!');
    });
  });
});
