import { Module } from '@nestjs/common';
import { OrderHistoryController } from './order-history.controller';
import { OrderHistoryService } from './order-history.service';

@Module({
  imports: [],
  controllers: [OrderHistoryController],
  providers: [OrderHistoryService],
})
export class OrderHistoryModule {}
