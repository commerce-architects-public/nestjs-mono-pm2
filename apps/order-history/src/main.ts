import { NestFactory } from '@nestjs/core';
import { OrderHistoryModule } from './order-history.module';

async function bootstrap() {
  const app = await NestFactory.create(OrderHistoryModule);
  await app.listen(5000);
}
bootstrap();
