import { CommonLibModule, CommonLibService } from '@app/common-lib';
import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
  imports: [CommonLibModule],
  controllers: [UsersController],
  providers: [UsersService, CommonLibService],
})
export class UsersModule {}
