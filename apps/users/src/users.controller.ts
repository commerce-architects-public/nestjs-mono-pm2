import { CommonLibService } from '@app/common-lib';
import { Controller, Get } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller()
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly commonLibService: CommonLibService,
  ) {}

  @Get()
  async getHello(): Promise<string> {
    console.log(
      'you are now logged in! ' +
        (await this.commonLibService.authenticate('foo', 'bar')),
    );
    return this.usersService.getHello();
  }

  @Get('/health')
  health() {
    return { health: 'ok' };
  }
}
